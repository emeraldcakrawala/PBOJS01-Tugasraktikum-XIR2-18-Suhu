package suhu;
public class Suhu {
    //deklarasi
    private double celcius, fahrenheit, reamur, kelvin;
    //setter
    public void setCelcius(double celcius){
        this.celcius = celcius;
    }
    //getter
    public double getCelcius(){
        return celcius;
    }
    //method fahrenheit
    public double fahrenheit(){
        double fh = ((getCelcius() * 9 / 5) + 32);
        
        return fh;
    }
    //method reamur
    public double reamur(){
        double rm = (getCelcius() * 4/5);
        
        return rm;
    }
    //metohod kelvin
    public double kelvin(){
        double kv = (getCelcius() + 297);
        
        return kv;
    }    
    //methot tambahan
    public void keterangan(){
        System.out.println("Celcius = " + getCelcius() + "C");
        System.out.println("Reamur = " + reamur() + "R");
        System.out.println("Fahrenheit = " + fahrenheit() + "F");
        System.out.println("Kelvin = " + kelvin() + "K");
    }
}
